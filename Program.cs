﻿using System.Runtime.InteropServices;

[DllImport("target/release/libraw_csharp")]
[return: MarshalAs(UnmanagedType.LPUTF8Str)]
static extern string reverse_inptr([MarshalAs(UnmanagedType.LPUTF8Str)] string text);

var quote = "« All that we see or seem is but a dream within a dream. » EAP";
var p = reverse_inptr(quote);

// See https://aka.ms/new-console-template for more information
Console.WriteLine("Hello, World: " + p);

quote = "Some other wise quote.";
p = reverse_inptr(quote);

// See https://aka.ms/new-console-template for more information
Console.WriteLine("Hello, World: " + p);
