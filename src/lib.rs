use std::ffi::CStr;
use std::ffi::CString;
use std::os::raw::c_char;

fn reverse(text: String) -> String {
    let s = text.chars().rev().collect::<String>();
    s
}

#[no_mangle]
pub extern "C" fn reverse_inptr(text_pointer: *const c_char) -> *const c_char {
    let c_str = unsafe { CStr::from_ptr(text_pointer) };
    let r_str = c_str.to_str().unwrap();
    let text = r_str.to_string();
    let reversed = reverse(text);
    let raw = CString::new(reversed).expect("CString::new failed!");
    raw.into_raw()
}
